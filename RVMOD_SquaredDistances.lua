RVMOD_SquaredDistances								= {}
local RVMOD_SquaredDistances						= RVMOD_SquaredDistances

local RVName										= "SquaredDistances"
local RVCredits										= nil
local RVLicense										= "MIT License"
local RVProjectURL									= "http://www.returnofreckoning.com/forum/viewtopic.php?f=11&t=4534"
local RVRecentUpdates								= 
"09.07.2015 - v1.01 Release\n"..
"\t- Project official site location has been changed\n"..
"\n"..
"24.11.2010 - v1.0 Release\n"..
"\t- Distance labels should update on change game settings now\n"..
"\n"..
"19.09.2010 - v0.6 Beta\n"..
"\t- SquaredDistances should show random information in test mode\n"..
"\t- /reloadui issue has been fixed. It should show all the distances after this command\n"..
"\t- Options are now available\n"..
"\n"..
"12.09.2010 - v0.5 Beta\n"..
"\t- Small improvements and optimizations been made\n"..
"\n"..
"05.09.2010 - v0.4 Beta\n"..
"\t- Initial upload"

local WindowSquaredDistancesSettings				= "RVMOD_SquaredDistancesSettingsWindow"

local ScreenMouseX									= 0
local ScreenMouseY									= 0
local IsMovingJoystickPointer						= false

local OnUpdateTimer									= 0
local SaveSettingsUpdateTimer						= 0
local IsSavingSettings								= false

local SquaredUnits									= {}
local SquaredLabels									= {}
local SquaredMode									= ""
local SquaredSettings								= {}
SquaredSettings["squared-distances-delay"]			= 0
SquaredSettings["squared-distances-font"]			= "font_heading_tiny_no_shadow"
SquaredSettings["squared-distances-useglobalscale"]	= false
SquaredSettings["squared-distances-scale"]			= 1
SquaredSettings["squared-distances-layer"]			= Window.Layers.OVERLAY
SquaredSettings["squared-distances-color"]			= {255, 255, 255}
SquaredSettings["squared-distances-color-alpha"]	= 1
SquaredSettings["squared-distances-anchorpoint"]	= "bottomleft"
SquaredSettings["squared-distances-position-x"]		= 5
SquaredSettings["squared-distances-position-y"]		= -3

local SDFonts										= {
--[[
	{FontIndex = "font_arial09",						FontName = "Arial 9pt"},
	{FontIndex = "font_arial11",						FontName = "Arial 11pt"},
	{FontIndex = "font_arial14",						FontName = "Arial 14pt"},
	{FontIndex = "font_arial18",						FontName = "Arial 18pt"},
	{FontIndex = "font_arial35_bold",					FontName = "Arial 32pt"},
]]
	{FontIndex = "font_journal_body",					FontName = "CronosPro-Regular 18pt"},
	{FontIndex = "font_journal_text",					FontName = "CronosPro-Regular 22pt"},
	{FontIndex = "font_journal_body_large",				FontName = "CronosPro-Regular 21pt"},
	{FontIndex = "font_journal_text_large",				FontName = "CronosPro-Regular 23pt"},
	{FontIndex = "font_journal_text_huge",				FontName = "CronosPro-Regular 26pt"},
	{FontIndex = "font_journal_text_italic",			FontName = "CronosPro-Italic 22pt"},

	{FontIndex = "font_clear_tiny",						FontName = "MyriadPro-Regular 14pt"},
	{FontIndex = "font_clear_small",					FontName = "MyriadPro-Regular 16pt"},
	{FontIndex = "font_guild_MP_R_17",					FontName = "MyriadPro-Regular 17pt"},
	{FontIndex = "font_clear_default",					FontName = "MyriadPro-Regular 18pt"},
	{FontIndex = "font_chat_text_no_outline",			FontName = "MyriadPro-Regular 18pt No Outline"},
	{FontIndex = "font_guild_MP_R_19",					FontName = "MyriadPro-Regular 19pt"},
	{FontIndex = "font_clear_medium",					FontName = "MyriadPro-Regular 20pt"},
	{FontIndex = "font_guild_MP_R_23",					FontName = "MyriadPro-Regular 23pt"},
	{FontIndex = "font_clear_large",					FontName = "MyriadPro-Regular 24pt"},
	{FontIndex = "font_clear_small_bold",				FontName = "MyriadPro-Bold 16pt"},
	{FontIndex = "font_chat_text_bold",					FontName = "MyriadPro-Bold 18pt"},
	{FontIndex = "font_clear_medium_bold",				FontName = "MyriadPro-Bold 20pt"},
	{FontIndex = "font_clear_large_bold",				FontName = "MyriadPro-Bold 24pt"},
	{FontIndex = "font_name_plate_names",				FontName = "MyriadPro-Bold 26pt"},

	{FontIndex = "font_alert_outline_half_tiny",		FontName = "CaslonAntiqueVL 9pt"},
	{FontIndex = "font_alert_outline_half_small",		FontName = "CaslonAntiqueVL 12pt"},
	{FontIndex = "font_alert_outline_half_medium",		FontName = "CaslonAntiqueVL 15pt"},
	{FontIndex = "font_alert_outline_tiny",				FontName = "CaslonAntiqueVL 18pt"},
	{FontIndex = "font_heading_tiny_no_shadow",			FontName = "CaslonAntiqueVL 18pt No Shadow"},
	{FontIndex = "font_heading_20pt_no_shadow",			FontName = "CaslonAntiqueVL 20pt No Shadow"},
	{FontIndex = "font_alert_outline_half_huge",		FontName = "CaslonAntiqueVL 21pt"},
	{FontIndex = "font_heading_22pt_no_shadow",			FontName = "CaslonAntiqueVL 22pt No Shadow"},
	{FontIndex = "font_default_medium_heading",			FontName = "CaslonAntiqueVL 23pt"},
	{FontIndex = "font_alert_outline_small",			FontName = "CaslonAntiqueVL 24pt"},
	{FontIndex = "font_heading_small_no_shadow",		FontName = "CaslonAntiqueVL 24pt No Shadow"},
	{FontIndex = "font_heading_zone_name_no_shadow",	FontName = "CaslonAntiqueVL 26pt No Shadow"},
	{FontIndex = "font_alert_outline_medium",			FontName = "CaslonAntiqueVL 30pt"},
	{FontIndex = "font_heading_default_no_shadow",		FontName = "CaslonAntiqueVL 30pt No Shadow"},
	{FontIndex = "font_alert_outline_large",			FontName = "CaslonAntiqueVL 36pt"},
	{FontIndex = "font_heading_medium",					FontName = "CaslonAntiqueVL 40pt"},
	{FontIndex = "font_heading_medium_noshadow",		FontName = "CaslonAntiqueVL 40pt No Shadow"},
	{FontIndex = "font_alert_outline_huge",				FontName = "CaslonAntiqueVL 42pt"},
	{FontIndex = "font_alert_outline_giant",			FontName = "CaslonAntiqueVL 48pt"},
	{FontIndex = "font_heading_large_noshadow",			FontName = "CaslonAntiqueVL 48pt No Shadow"},
	{FontIndex = "font_alert_outline_gigantic",			FontName = "CaslonAntiqueVL 60pt"},
	{FontIndex = "font_heading_big_noshadow",			FontName = "CaslonAntiqueVL 60pt No Shadow"},
	{FontIndex = "font_heading_huge_no_shadow",			FontName = "CaslonAntiqueVL 63pt No Shadow"},
	{FontIndex = "font_heading_huge",					FontName = "CaslonAntiqueVL 72pt"},
	{FontIndex = "font_heading_huge_noshadow",			FontName = "CaslonAntiqueVL 72pt No Shadow"},

	{FontIndex = "font_name_plate_titles_old",			FontName = "AgeOfReckoning 15pt"},
	{FontIndex = "font_default_text_small",				FontName = "AgeOfReckoning 17pt"},
	{FontIndex = "font_default_text",					FontName = "AgeOfReckoning 18pt"},
	{FontIndex = "font_default_text_no_outline",		FontName = "AgeOfReckoning 18pt No Outline"},
	{FontIndex = "font_heading_target_mouseover_name",	FontName = "AgeOfReckoning 19pt"},
	{FontIndex = "font_name_plate_names_old",			FontName = "AgeOfReckoning 20pt"},
	{FontIndex = "font_default_sub_heading_no_outline",	FontName = "AgeOfReckoning 20pt No Outline"},
	{FontIndex = "font_journal_small_heading",			FontName = "AgeOfReckoning 22pt"},
	{FontIndex = "font_default_war_heading",			FontName = "AgeOfReckoning 23pt"},
	{FontIndex = "font_journal_heading_smaller",		FontName = "AgeOfReckoning 24pt"},
	{FontIndex = "font_default_text_huge",				FontName = "AgeOfReckoning 30pt"},
	{FontIndex = "font_default_text_gigantic",			FontName = "AgeOfReckoning 48pt"},
--[[
	{FontIndex = "font_chat_window_game_rating_text",	FontName = "2002L_NHN 18pt"},
	{FontIndex = "font_title_window_game_rating_text",	FontName = "2002L_NHN 30pt"},
]]
}

local SDAnchorPoints								= {
	{AnchorPointIndex = "topleft",						AnchorPointName = "Top Left"},
	{AnchorPointIndex = "top",							AnchorPointName = "Top"},
	{AnchorPointIndex = "topright",						AnchorPointName = "Top Right"},
	{AnchorPointIndex = "left",							AnchorPointName = "Left"},
	{AnchorPointIndex = "center",						AnchorPointName = "Center"},
	{AnchorPointIndex = "right",						AnchorPointName = "Right"},
	{AnchorPointIndex = "bottomleft",					AnchorPointName = "Bottom Left"},
	{AnchorPointIndex = "bottom",						AnchorPointName = "Bottom"},
	{AnchorPointIndex = "bottomright",					AnchorPointName = "Bottom Right"},
}

local SDLayers										= {
	{LayerIndex = Window.Layers.BACKGROUND,				LayerName = "Background Layer (0)"},
	{LayerIndex = Window.Layers.DEFAULT,				LayerName = "Default Layer (1)"},
	{LayerIndex = Window.Layers.SECONDARY,				LayerName = "Secondary Layer (2)"},
	{LayerIndex = Window.Layers.POPUP,					LayerName = "Popup Layer (3)"},
	{LayerIndex = Window.Layers.OVERLAY,				LayerName = "Overlay Layer (4)"},
}

--------------------------------------------------------------
-- var DefaultConfiguration
-- Description: default module configuration
--------------------------------------------------------------
RVMOD_SquaredDistances.DefaultConfiguration	=
{

}

--------------------------------------------------------------
-- var CurrentConfiguration
-- Description: current module configuration
--------------------------------------------------------------
RVMOD_SquaredDistances.CurrentConfiguration	=
{
	-- should stay empty, will load in the InitializeConfiguration() function
}

--------------------------------------------------------------
-- function Initialize()
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.Initialize()

	-- First step: load configuration
	RVMOD_SquaredDistances.InitializeConfiguration()

	-- Second step: define event handlers
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, 	"RVMOD_SquaredDistances.OnAllModulesInitialized")
	RegisterEventHandler(SystemData.Events.L_BUTTON_UP_PROCESSED,		"RVMOD_SquaredDistances.OnLButtonUpJoystick")
	RegisterEventHandler(SystemData.Events.USER_SETTINGS_CHANGED,		"RVMOD_SquaredDistances.OnUserSettingsChanged")
end

--------------------------------------------------------------
-- function Shutdown()
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.Shutdown()

	-- First step: destroy settings window
	if DoesWindowExist(WindowSquaredDistancesSettings) then
		DestroyWindow(WindowSquaredDistancesSettings)
	end

	-- Second step: unregister events
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED,	"RVMOD_SquaredDistances.OnAllModulesInitialized")
	UnregisterEventHandler(SystemData.Events.L_BUTTON_UP_PROCESSED,		"RVMOD_SquaredDistances.OnLButtonUpJoystick")
	UnregisterEventHandler(SystemData.Events.USER_SETTINGS_CHANGED,		"RVMOD_SquaredDistances.OnUserSettingsChanged")
end

--------------------------------------------------------------
-- function InitializeConfiguration()
-- Description: loads current configuration
--------------------------------------------------------------
function RVMOD_SquaredDistances.InitializeConfiguration()

	-- First step: move default value to the CurrentConfiguration variable
	for k,v in pairs(RVMOD_SquaredDistances.DefaultConfiguration) do
		if(RVMOD_SquaredDistances.CurrentConfiguration[k]==nil) then
			RVMOD_SquaredDistances.CurrentConfiguration[k]=v
		end
	end
end

--------------------------------------------------------------
-- function OnAllModulesInitialized()
-- Description: 
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnAllModulesInitialized()

	-- First step: load settings from the squared
	for VName, VValue in pairs(SquaredSettings) do
		SquaredSettings[VName] = Squared.GetSetting(VName) or VValue
	end

	-- Second step: lets register some event handlers in the squared
	Squared.RegisterEventHandler("setname",		RVMOD_SquaredDistances.OnSquaredSetName)
	Squared.RegisterEventHandler("changemode",	RVMOD_SquaredDistances.OnSquaredChangeMode)
	Squared.RegisterEventHandler("setsetting",	RVMOD_SquaredDistances.OnSquaredSetSetting)

	-- Third step: (hack) reset mode once more again.
	--				We need to do that since if we use the "/reloadui" command "Squared" won't do that itself
	Squared.ResetMode()

	-- Final step: register in the RV Mods Manager
	-- Please note the folowing:
	-- 1. always do this ON SystemData.Events.ALL_MODULES_INITIALIZED event
	-- 2. you don't need to add RVMOD_Manager to the dependency list
	-- 3. the registration code should be same as below, with your own function parameters
	-- 4. for more information please follow by project official site
	if RVMOD_Manager then
		RVMOD_Manager.API_RegisterAddon("RVMOD_SquaredDistances", RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnRVManagerCallback)
	end
end

--------------------------------------------------------------
-- function OnRVManagerCallback
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnRVManagerCallback(Self, Event, EventData)

	if		Event == RVMOD_Manager.Events.NAME_REQUESTED then

		return RVName

	elseif	Event == RVMOD_Manager.Events.CREDITS_REQUESTED then

		return RVCredits

	elseif	Event == RVMOD_Manager.Events.LICENSE_REQUESTED then

		return RVLicense

	elseif	Event == RVMOD_Manager.Events.PROJECT_URL_REQUESTED then

		return RVProjectURL

	elseif	Event == RVMOD_Manager.Events.RECENT_UPDATES_REQUESTED then

		return RVRecentUpdates

	elseif	Event == RVMOD_Manager.Events.PARENT_WINDOW_UPDATED then

		if not DoesWindowExist(WindowSquaredDistancesSettings) then
			RVMOD_SquaredDistances.InitializeSettingsWindow()
		end

		WindowSetParent(WindowSquaredDistancesSettings, EventData.ParentWindow)
		WindowClearAnchors(WindowSquaredDistancesSettings)
		WindowAddAnchor(WindowSquaredDistancesSettings, "topleft", EventData.ParentWindow, "topleft", 0, 0)
		WindowAddAnchor(WindowSquaredDistancesSettings, "bottomright", EventData.ParentWindow, "bottomright", 0, 0)

		RVMOD_SquaredDistances.UpdateWindowSettings()

		return true

	end
end

--------------------------------------------------------------
-- function InitializeSettingsWindow()
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.InitializeSettingsWindow()

	-- First step: create main window
	CreateWindow(WindowSquaredDistancesSettings, true)

	-- Second step: set delay slider texts
	LabelSetText(WindowSquaredDistancesSettings.."SliderDelayMinLabel", L"0 sec")
	LabelSetText(WindowSquaredDistancesSettings.."SliderDelayMidLabel", L"5 sec")
	LabelSetText(WindowSquaredDistancesSettings.."SliderDelayMaxLabel", L"10 sec")

	LabelSetText(WindowSquaredDistancesSettings.."LabelImportantInformation", L"ATTENTION!!! A small \"delay\" value may harm your game performance, but will increase output quality.")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelImportantInformation", DefaultColor.RED.r, DefaultColor.RED.g, DefaultColor.RED.b)

	-- Third step: set scale slider texts
	LabelSetText(WindowSquaredDistancesSettings.."SliderScaleMinLabel", L"0.5")
	LabelSetText(WindowSquaredDistancesSettings.."SliderScaleMidLabel", L"1")
	LabelSetText(WindowSquaredDistancesSettings.."SliderScaleMaxLabel", L"1.5")

	LabelSetText(WindowSquaredDistancesSettings.."LabelUseGlobalScale", L"Use Global UI Scale")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelUseGlobalScale", 255, 255, 255)

	-- Fourth step: set layers combobox
	LabelSetText(WindowSquaredDistancesSettings.."LabelLayers", L"Layer")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelLayers", 255, 255, 255)
	for LayerIndex, LayerData in ipairs(SDLayers) do

		ComboBoxAddMenuItem(WindowSquaredDistancesSettings.."ComboBoxLayers", towstring(LayerData.LayerName))
	end

	-- Fifth step: set anchor points combobox
	LabelSetText(WindowSquaredDistancesSettings.."LabelAnchorPoints", L"Anchor Point")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelAnchorPoints", 255, 255, 255)
	for AnchorPointIndex, AnchorPointData in ipairs(SDAnchorPoints) do

		ComboBoxAddMenuItem(WindowSquaredDistancesSettings.."ComboBoxAnchorPoints", towstring(AnchorPointData.AnchorPointName))
	end

	-- Sixth step: set fonts combobox
	LabelSetText(WindowSquaredDistancesSettings.."LabelFonts", L"Font")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelFonts", 255, 255, 255)
	for FontIndex, FontData in ipairs(SDFonts) do

		ComboBoxAddMenuItem(WindowSquaredDistancesSettings.."ComboBoxFonts", towstring(FontData.FontName))
	end

	-- Seventh step: set colorbox texts
	LabelSetText(WindowSquaredDistancesSettings.."LabelColor", L"Color")
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelColor", 255, 255, 255)

	-- Eight step: set joystick information
	AnimatedImageStartAnimation(WindowSquaredDistancesSettings.."ImageAnimation", 0, true, false, 0)
	LabelSetTextColor(WindowSquaredDistancesSettings.."LabelCoordinates", 107, 79, 51)
end

--------------------------------------------------------------
-- function UpdateWindowSettings()
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.UpdateWindowSettings()

	-- : show update delay
	SliderBarSetCurrentPosition(WindowSquaredDistancesSettings.."SliderDelay", SquaredSettings["squared-distances-delay"] / 10)

	-- : show scale information
	ButtonSetPressedFlag(WindowSquaredDistancesSettings.."CheckBoxUseGlobalScale", SquaredSettings["squared-distances-useglobalscale"])
	SliderBarSetCurrentPosition(WindowSquaredDistancesSettings.."SliderScale", SquaredSettings["squared-distances-scale"] - 0.5)

	-- : show layer
	local LayerIndex = RVMOD_SquaredDistances.GetLayerIndex(SquaredSettings["squared-distances-layer"])
	if LayerIndex > 0 then
		ComboBoxSetSelectedMenuItem(WindowSquaredDistancesSettings.."ComboBoxLayers", LayerIndex)
	end

	-- : show anchor point
	local AnchorPointIndex = RVMOD_SquaredDistances.GetAnchorPointIndex(SquaredSettings["squared-distances-anchorpoint"])
	if AnchorPointIndex > 0 then
		ComboBoxSetSelectedMenuItem(WindowSquaredDistancesSettings.."ComboBoxAnchorPoints", AnchorPointIndex)
	end

	-- : show font
	local FontIndex = RVMOD_SquaredDistances.GetFontIndex(SquaredSettings["squared-distances-font"])
	if FontIndex > 0 then
		ComboBoxSetSelectedMenuItem(WindowSquaredDistancesSettings.."ComboBoxFonts", FontIndex)
	end

	-- : show color
	WindowSetTintColor(WindowSquaredDistancesSettings.."ColorBorderForeground", SquaredSettings["squared-distances-color"][1], SquaredSettings["squared-distances-color"][2], SquaredSettings["squared-distances-color"][3])

	-- : show position
	LabelSetText(WindowSquaredDistancesSettings.."LabelCoordinates", SquaredSettings["squared-distances-position-x"]..L" : "..SquaredSettings["squared-distances-position-y"])
end

--------------------------------------------------------------
-- function OnUpdate
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnUpdate(timePassed)

	-- First step: update local timer
	OnUpdateTimer = OnUpdateTimer + timePassed

	-- First step: check for mouse position changed
	if (ScreenMouseX ~= SystemData.MousePosition.x or ScreenMouseY ~= SystemData.MousePosition.y) then

		-- Second step: check if joystick in action
		if IsMovingJoystickPointer then

			-- : send a position difference
			RVMOD_SquaredDistances.MoveLabelPosition(SystemData.MousePosition.x - ScreenMouseX, SystemData.MousePosition.y - ScreenMouseY)
		end

		-- Third step: save new position
		ScreenMouseX = SystemData.MousePosition.x
		ScreenMouseY = SystemData.MousePosition.y
	end

	-- : check if we need to send settings to the squared
	if OnUpdateTimer - SaveSettingsUpdateTimer  >= 1 and IsSavingSettings then

		-- : send settings to the squared
		Squared.MassSetSettings(SquaredSettings)

		-- : tell everyone, settings are saved now
		IsSavingSettings = false
	end
end

--------------------------------------------------------------
-- function OnSquaredSetName
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnSquaredSetName(UnitFrame)

	-- : define locals
	local UnitName = wstring.gsub(UnitFrame.name, L"(^.)", L"")

	-- : check if window name been changed
	if		SquaredUnits[UnitName] and
			SquaredUnits[UnitName] ~= UnitFrame.m_Name or
		not SquaredLabels[UnitFrame.m_Name] or
			SquaredLabels[UnitFrame.m_Name] ~= UnitName then

		-- : clear the old name
		SquaredUnits[UnitName] = nil

		-- : unregister target from the RVAPI_Range
		RVAPI_Range.API_UnregisterTarget(RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnTargetRangeUpdated, UnitName)
	end

	-- : check if target is not registered yet
	if	not SquaredUnits[UnitName] then

		-- : check if window already exists
		if not DoesWindowExist(UnitFrame.m_Name.."Distance") then

			-- : create a distance label
			CreateWindowFromTemplate(UnitFrame.m_Name.."Distance", "RVMOD_SquaredDistancesTemplate", UnitFrame.m_Name)

			-- : set a labels settings
			RVMOD_SquaredDistances.SetLabelSettings(UnitFrame.m_Name)
		end

		-- : save new created window name
		SquaredLabels[UnitFrame.m_Name] = UnitName

		-- : save window name
		SquaredUnits[UnitName] = UnitFrame.m_Name

		-- : register/update target in the RVAPI_Range
		RVAPI_Range.API_RegisterTarget(RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnTargetRangeUpdated, UnitName, SquaredSettings["squared-distances-delay"], RVAPI_Range.DelayType.DELAY_EXACT_TIME, UnitName)
	end
end

--------------------------------------------------------------
-- function OnSquaredChangeMode
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnSquaredChangeMode(ModeName)

	-- First step: check for the modes difference
	if SquaredMode ~= ModeName then

		-- Second step: save a new mode name
		SquaredMode = ModeName

		-- Third step: unregister all targets from the RVAPI_Range
		RVAPI_Range.API_UnregisterTarget(RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnTargetRangeUpdated, nil)

		-- Fourth step: clear the registered SquaredUnits list
		SquaredUnits = {}
	end
end

--------------------------------------------------------------
-- function OnSquaredSetSetting
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnSquaredSetSetting(Variable, Value)

	-- First step: check if this is our setting
	if SquaredSettings[Variable] ~= nil and not IsSavingSettings then

		-- Second step: set new value
		SquaredSettings[Variable] = Value

		-- Third step: update settings window
		-- TODO: (MrAngel) I won't optimize this part now since this is not a runtime critical one
		if DoesWindowExist(WindowSquaredDistancesSettings) then
			RVMOD_SquaredDistances.UpdateWindowSettings()
		end

		-- Fourth step: update information
		if Variable == "squared-distances-delay" then

			-- : update all targets
			RVMOD_SquaredDistances.UpdateTargets()
		else

			-- : update all labels
			RVMOD_SquaredDistances.UpdateLabels()
		end
	end
end

--------------------------------------------------------------
-- function OnTargetRangeUpdated
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.OnTargetRangeUpdated(Self, Distances, RangeType, UnitName)

	-- First step: check if SquaredUnits is set
	if SquaredUnits[UnitName] and SquaredLabels[SquaredUnits[UnitName]] == UnitName then

		-- Second step: define locals
		local Distance

		-- Third step: define the distance value according to the current mode
		if SquaredMode == "testmode" then
			Distance	= math.random(0, 200)
		else
			Distance	= Distances[1].RangeMax
		end

		-- Fourth step: set new distance information
		if Distance > 50000 then

			-- : show "FAR" string if distance number is too large
			LabelSetText(SquaredUnits[UnitName].."Distance", L"FAR")
		else

			-- : show actual distance
			LabelSetText(SquaredUnits[UnitName].."Distance", towstring(Distance))
		end
	end
end

--------------------------------------------------------------
-- function MoveLabelPosition
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.MoveLabelPosition(OffsetX, OffsetY)

	-- : save new information
	SquaredSettings["squared-distances-position-x"]	= SquaredSettings["squared-distances-position-x"] + OffsetX
	SquaredSettings["squared-distances-position-y"]	= SquaredSettings["squared-distances-position-y"] + OffsetY

	-- : update all registered labels
	RVMOD_SquaredDistances.UpdateLabels()

	-- : show current position in the settings window
	LabelSetText(WindowSquaredDistancesSettings.."LabelCoordinates", SquaredSettings["squared-distances-position-x"]..L" : "..SquaredSettings["squared-distances-position-y"])

	-- : prepare for saving to the squared
	SaveSettingsUpdateTimer	= OnUpdateTimer
	IsSavingSettings		= true
end

--------------------------------------------------------------
-- function SetLabelSettings
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.SetLabelSettings(WindowName)

	-- : get locals
	local LabelName	= WindowName.."Distance"

	-- : set anchor and position
	WindowClearAnchors(LabelName)
	WindowAddAnchor(LabelName, SquaredSettings["squared-distances-anchorpoint"], WindowName, SquaredSettings["squared-distances-anchorpoint"], SquaredSettings["squared-distances-position-x"], SquaredSettings["squared-distances-position-y"])

	-- : set font
	LabelSetFont(LabelName, SquaredSettings["squared-distances-font"], WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)

	-- : set font align
	local TextAlign = SquaredSettings["squared-distances-anchorpoint"]
	if TextAlign == "left" then TextAlign = "leftcenter" end
	if TextAlign == "right" then TextAlign = "rightcenter" end
	if TextAlign == "topleft" then TextAlign = "left" end
	if TextAlign == "topright" then TextAlign = "right" end
	LabelSetTextAlign(LabelName, TextAlign)

	-- : set color
	LabelSetTextColor(LabelName, SquaredSettings["squared-distances-color"][1], SquaredSettings["squared-distances-color"][2], SquaredSettings["squared-distances-color"][3])
	WindowSetFontAlpha(LabelName, SquaredSettings["squared-distances-color-alpha"])

	-- : set layer
	WindowSetLayer(LabelName, SquaredSettings["squared-distances-layer"])

	-- : set scale
	local ScaleFactor
	if SquaredSettings["squared-distances-useglobalscale"] then
		ScaleFactor = InterfaceCore.GetScale()
	else
		ScaleFactor = SquaredSettings["squared-distances-scale"]
	end
	WindowSetScale(LabelName, ScaleFactor)
end

--------------------------------------------------------------
-- function UpdateLabels
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.UpdateLabels()

	-- : list all registered labels
	for WindowName, __ in pairs(SquaredLabels) do

		-- : check if label created
		if DoesWindowExist(WindowName.."Distance") then

			-- : update label with a new settings 
			RVMOD_SquaredDistances.SetLabelSettings(WindowName)
		end
	end
end

--------------------------------------------------------------
-- function UpdateTargets
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.UpdateTargets()

	-- : list all registered units in the RVAPI_Range and update it's timer delays
	for UnitName, __ in pairs(SquaredUnits) do

		-- : register/update target in the RVAPI_Range
		RVAPI_Range.API_RegisterTarget(RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnTargetRangeUpdated, UnitName, SquaredSettings["squared-distances-delay"], RVAPI_Range.DelayType.DELAY_EXACT_TIME, UnitName)
	end
end

--------------------------------------------------------------
-- function GetFontIndex
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.GetFontIndex(FontName)

	-- : list all registered fonts
	for FontIndex, FontData in ipairs(SDFonts) do

		-- : check for the name
		if FontData.FontIndex == FontName then

			-- : return result
			return FontIndex
		end
	end

	-- : return false result
	return 0
end

--------------------------------------------------------------
-- function GetAnchorPointIndex
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.GetAnchorPointIndex(AnchorPointName)

	-- : list all registered anchor points
	for AnchorPointIndex, AnchorPointData in ipairs(SDAnchorPoints) do

		-- : check for the name
		if AnchorPointData.AnchorPointIndex == AnchorPointName then

			-- : return result
			return AnchorPointIndex
		end
	end

	-- : return false result
	return 0
end

--------------------------------------------------------------
-- function GetLayerIndex
-- Description:
--------------------------------------------------------------
function RVMOD_SquaredDistances.GetLayerIndex(LayerName)

	-- : list all registered layers
	for LayerIndex, LayerData in ipairs(SDLayers) do

		-- : check for the name
		if LayerData.LayerIndex == LayerName then

			-- : return result
			return LayerIndex
		end
	end

	-- : return false result
	return 0
end

-------------------------------------------------------------
-- function 
-- Description: events
-------------------------------------------------------------

function RVMOD_SquaredDistances.OnSlideDistancesDelay(slidePos)

	-- : save new setting
	SquaredSettings["squared-distances-delay"] = slidePos * 10

	-- : update all registered labels
	RVMOD_SquaredDistances.UpdateTargets()

	-- : prepare for saving to the squared
	SaveSettingsUpdateTimer	= OnUpdateTimer
	IsSavingSettings		= true
end

function RVMOD_SquaredDistances.OnSlideScale(slidePos)

	-- : save new setting
	SquaredSettings["squared-distances-scale"] = slidePos + 0.5

	-- : update all registered labels
	RVMOD_SquaredDistances.UpdateLabels()

	-- : prepare for saving to the squared
	SaveSettingsUpdateTimer	= OnUpdateTimer
	IsSavingSettings		= true
end

function RVMOD_SquaredDistances.OnCheckBoxUseGlobalScale()

	-- : save new setting
	SquaredSettings["squared-distances-useglobalscale"] = not SquaredSettings["squared-distances-useglobalscale"]

	-- : update all registered labels
	RVMOD_SquaredDistances.UpdateLabels()

	-- : set the checkbox status
	ButtonSetPressedFlag(WindowSquaredDistancesSettings.."CheckBoxUseGlobalScale", SquaredSettings["squared-distances-useglobalscale"])

	-- : prepare for saving to the squared
	SaveSettingsUpdateTimer	= OnUpdateTimer
	IsSavingSettings		= true
end

function RVMOD_SquaredDistances.OnComboBoxLayerChange(choiceIndex)

	local LayerData = SDLayers[choiceIndex]
	if LayerData then

		-- : save new setting
		SquaredSettings["squared-distances-layer"] = LayerData.LayerIndex

		-- : update all registered labels
		RVMOD_SquaredDistances.UpdateLabels()

		-- : prepare for saving to the squared
		SaveSettingsUpdateTimer	= OnUpdateTimer
		IsSavingSettings		= true
	end
end

function RVMOD_SquaredDistances.OnComboBoxAnchorPointsChange(choiceIndex)

	local AnchorPointData = SDAnchorPoints[choiceIndex]
	if AnchorPointData then

		-- : save new setting
		SquaredSettings["squared-distances-anchorpoint"] = AnchorPointData.AnchorPointIndex

		-- : update all registered labels
		RVMOD_SquaredDistances.UpdateLabels()

		-- : prepare for saving to the squared
		SaveSettingsUpdateTimer	= OnUpdateTimer
		IsSavingSettings		= true
	end
end

function RVMOD_SquaredDistances.OnComboBoxFontsChange(choiceIndex)

	local FontData = SDFonts[choiceIndex]
	if FontData then

		-- : save new setting
		SquaredSettings["squared-distances-font"] = FontData.FontIndex

		-- : update all registered labels
		RVMOD_SquaredDistances.UpdateLabels()

		-- : prepare for saving to the squared
		SaveSettingsUpdateTimer	= OnUpdateTimer
		IsSavingSettings		= true
	end
end

function RVMOD_SquaredDistances.OnLButtonUpColorBox()

	local ColorDialogOwner, ColorDialogFunction = RVAPI_ColorDialog.API_GetLink()
	if ColorDialogOwner ~= RVMOD_SquaredDistances or ColorDialogFunction ~= RVMOD_SquaredDistances.OnColorDialogCallback then
		RVAPI_ColorDialog.API_OpenDialog(RVMOD_SquaredDistances, RVMOD_SquaredDistances.OnColorDialogCallback, true, SquaredSettings["squared-distances-color"][1], SquaredSettings["squared-distances-color"][2], SquaredSettings["squared-distances-color"][3], SquaredSettings["squared-distances-color-alpha"], Window.Layers.SECONDARY)
	else
		RVAPI_ColorDialog.API_CloseDialog(true)
	end
end

function RVMOD_SquaredDistances.OnColorDialogCallback(Self, Event, EventData)

	-- First step: check for the right event
	if Event == RVAPI_ColorDialog.Events.COLOR_EVENT_UPDATED then

		-- : save new setting
		SquaredSettings["squared-distances-color"]			= {EventData.Red, EventData.Green, EventData.Blue}
		SquaredSettings["squared-distances-color-alpha"]	= EventData.Alpha

		-- : update all registered labels
		RVMOD_SquaredDistances.UpdateLabels()

		-- : set color box value
		WindowSetTintColor(WindowSquaredDistancesSettings.."ColorBorderForeground", EventData.Red, EventData.Green, EventData.Blue)

		-- : prepare for saving to the squared
		SaveSettingsUpdateTimer	= OnUpdateTimer
		IsSavingSettings		= true
	end
end

function RVMOD_SquaredDistances.OnLButtonUpJoystick()

	-- : inform everyone we are not moving pointer
	IsMovingJoystickPointer = false
end

function RVMOD_SquaredDistances.OnLButtonDownJoystick()

	-- : inform everyone we are starting to move pointer
	IsMovingJoystickPointer = true
end

function RVMOD_SquaredDistances.OnLButtonUpLeft()

	RVMOD_SquaredDistances.MoveLabelPosition(-1, 0)
end

function RVMOD_SquaredDistances.OnLButtonUpRight()

	RVMOD_SquaredDistances.MoveLabelPosition(1, 0)
end

function RVMOD_SquaredDistances.OnLButtonUpTop()

	RVMOD_SquaredDistances.MoveLabelPosition(0, -1)
end

function RVMOD_SquaredDistances.OnLButtonUpBottom()

	RVMOD_SquaredDistances.MoveLabelPosition(0, 1)
end

function RVMOD_SquaredDistances.OnMouseOverJoystick()

	local StartAlpha = 0

	WindowStopAlphaAnimation(WindowSquaredDistancesSettings.."ImageAnimation")
	StartAlpha = WindowGetAlpha(WindowSquaredDistancesSettings.."ImageAnimation")
	WindowStartAlphaAnimation(WindowSquaredDistancesSettings.."ImageAnimation", Window.AnimationType.SINGLE_NO_RESET, StartAlpha-0.000001, 0.0, 0.5, false, 0, 0)

	WindowStopAlphaAnimation(WindowSquaredDistancesSettings.."ImageClickAndDrag")
	StartAlpha = WindowGetAlpha(WindowSquaredDistancesSettings.."ImageClickAndDrag")
	WindowStartAlphaAnimation(WindowSquaredDistancesSettings.."ImageClickAndDrag", Window.AnimationType.SINGLE_NO_RESET, StartAlpha, 1, 0.5, false, 0, 0)
end

function RVMOD_SquaredDistances.OnMouseOverEndJoystick()

	local StartAlpha = 0

	WindowStopAlphaAnimation(WindowSquaredDistancesSettings.."ImageAnimation")
	StartAlpha = WindowGetAlpha(WindowSquaredDistancesSettings.."ImageAnimation")
	WindowStartAlphaAnimation(WindowSquaredDistancesSettings.."ImageAnimation", Window.AnimationType.SINGLE_NO_RESET, StartAlpha, 1, 0.5, false, 0, 0)

	WindowStopAlphaAnimation(WindowSquaredDistancesSettings.."ImageClickAndDrag")
	StartAlpha = WindowGetAlpha(WindowSquaredDistancesSettings.."ImageClickAndDrag")
	WindowStartAlphaAnimation(WindowSquaredDistancesSettings.."ImageClickAndDrag", Window.AnimationType.SINGLE_NO_RESET, StartAlpha-0.000001, 0.0, 0.5, false, 0, 0)
end

function RVMOD_SquaredDistances.OnUserSettingsChanged()

	-- : update all registered labels
	RVMOD_SquaredDistances.UpdateLabels()
end